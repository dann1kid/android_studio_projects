package com.example.myapplication.ui.yt

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.MediaKeys.R
import com.example.MediaKeys.sendUDPB as sendB


class YtFragment : Fragment() {

    private lateinit var dashboardViewModel: YTViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dashboardViewModel =
            ViewModelProviders.of(this).get(YTViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_yt, container, false)

        val butYTPlay: Button = root.findViewById(R.id.buttonYTPlay)
        butYTPlay.setOnClickListener{
            val bText = 132
            sendB(bText)
        }

        val butRew10: Button = root.findViewById(R.id.buttonYTRew10)
        butRew10.setOnClickListener{
            val bText = 131
            sendB(bText)
        }

        val butFF10: Button = root.findViewById(R.id.buttonYT1FF10)
        butFF10.setOnClickListener{
            val bText = 133
            sendB(bText)
        }

        val butLouder: Button = root.findViewById(R.id.buttonYTLouder)
        butLouder.setOnClickListener{
            val bText = 110
            sendB(bText)
        }

        val butLower: Button = root.findViewById(R.id.buttonYTLower)
        butLower.setOnClickListener{
            val bText = 24
            sendB(bText)
        }

        val butMute: Button = root.findViewById(R.id.buttonYTMute)
        butMute.setOnClickListener{
            val bText = 134
            sendB(bText)
        }

        val butFF: Button = root.findViewById(R.id.buttonYTIncreaseSpeed)
        butFF.setOnClickListener{
            val bText = 150
            sendB(bText)
        }

        val butFB: Button = root.findViewById(R.id.buttonYTDecreaseSpeed)
        butFB.setOnClickListener{
            val bText = 148
            sendB(bText)
        }

        val butNextVid: Button = root.findViewById(R.id.buttonYTNextTrack)
        butNextVid.setOnClickListener{
            val bText = 151
            sendB(bText)
        }

        val butPrevVid: Button = root.findViewById(R.id.buttonYTPrevTrack)
        butPrevVid.setOnClickListener{
            val bText = 152                     //need to translate hotkey Shift + n, and previous not work, so
            sendB(bText)
        }

        return root
    }

}