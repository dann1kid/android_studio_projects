@file:Suppress("NAME_SHADOWING")

package com.example.MediaKeys

import android.os.Bundle
import android.os.StrictMode
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress


/*
init
 */

class SoftOptions() {
    var remoteHost: String = "255.255.255.255"
    var remotePort: Int = 54679
    // settings constructor
}

// Global
val Settings = SoftOptions()

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        val policy =
            StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)


    }

}

/*
broadcast fun
*/
fun sendUDPB(msg: Any) {
    val clientSocket = DatagramSocket()
    clientSocket.broadcast = true
    val msg = msg.toString()
    val sendData = msg.toByteArray()

    val sendPack = DatagramPacket(
        sendData, sendData.size, InetAddress.getByName(Settings.remoteHost), Settings.remotePort
    )
    clientSocket.send(sendPack)
}