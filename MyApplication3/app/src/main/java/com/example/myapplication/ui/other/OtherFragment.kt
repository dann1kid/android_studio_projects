package com.example.myapplication.ui.other

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.MediaKeys.R
import com.example.MediaKeys.sendUDPB as sendB

class OtherFragment : Fragment() {

    private lateinit var notificationsViewModel: OtherPlayersModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        notificationsViewModel =
            ViewModelProviders.of(this).get(OtherPlayersModel::class.java)
        val root = inflater.inflate(R.layout.fragment_other, container, false)

        val butLeft: Button = root.findViewById(R.id.buttonOtherFBLeft)
        butLeft.setOnClickListener{
            val bText = 69
            sendB(bText)
        }

        val butRight: Button = root.findViewById(R.id.buttonOtherFFRight)
        butRight.setOnClickListener{
            val bText = 98
            sendB(bText)
        }

        val butSpace: Button = root.findViewById(R.id.buttonOtherPlay)
        butSpace.setOnClickListener{
            val bText = 106
            sendB(bText)
        }


        return root
    }
}