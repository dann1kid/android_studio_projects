package com.example.myapplication.ui.music

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.MediaKeys.sendUDPB as sendB
import com.example.MediaKeys.R


class MusicFragment : Fragment() {

    private lateinit var musicViewModel: MusicViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        musicViewModel =
            ViewModelProviders.of(this).get(MusicViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_music, container, false)

        val butMusPlay:Button = root.findViewById(R.id.buttonPlayMusic)
        butMusPlay.setOnClickListener{
            val bText = 90
            sendB(bText)
        }

        val butMusRewind:Button = root.findViewById(R.id.buttonPrevMusic)
        butMusRewind.setOnClickListener{
            val bText = 91
            sendB(bText)
        }

        val butMusForward:Button = root.findViewById(R.id.buttonNextMusic)
        butMusForward.setOnClickListener{
            val bText = 72
            sendB(bText)
        }

        val butMediaLouder:Button = root.findViewById(R.id.buttonLouder)
        butMediaLouder.setOnClickListener{
            val bText = 113
            sendB(bText)
        }

        val butMediaLower:Button = root.findViewById(R.id.buttonLower)
        butMediaLower.setOnClickListener{
            val bText = 111
            sendB(bText)
        }

        val butMediaMute:Button = root.findViewById(R.id.buttonMute)
        butMediaMute.setOnClickListener{
            val bText = 112
            sendB(bText)
        }

        return root
    }
}