
package com.example.mediakeys

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_main.*
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress

/*
init
 */

class SoftOptions() {
    var remoteHost: String = "255.255.255.255"
    var remotePort: Int = 54679
}

// Global
val Settings = SoftOptions()

open class MainActivity : AppCompatActivity() {
    /*
 UI activity
  */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //buttonBroadcast()
        buttonVolDownFun()
        buttonVolUpFun()
        buttonMuteFun()
        buttonNextTrackFun()
        buttonPlayFun()
        buttonPrevTrackFun()

        // TODO: Search UIRunnable
    }


    /*
    edit list SAMPLE
     *//*
    fun fillPCList() {
        var ls = listOf("item_0", "item_1", "item_2")
        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, ls)
        lv.adapter = adapter
    }
    */


    /*
    broadcast
     */
    private fun sendUDPB(msg: Any) {
        val clientSocket = DatagramSocket()
        clientSocket.broadcast = true
        val msg = msg.toString()
        val sendData = msg.toByteArray()
        /*
        заменить в sendPack аргумент [2] на ф-цию getBroadcastAddress()
         */
        val sendPack = DatagramPacket(
            sendData, sendData.size, InetAddress.getByName(Settings.remoteHost), Settings.remotePort
        )
        clientSocket.send(sendPack)
    }


    /*
    udp inf receive
     */

    open fun receiveUDP() {
        val buffer = ByteArray(8)
        var socket: DatagramSocket? = null
        try {
            //Keep a socket open to listen to all the UDP traffic that is destined for this port
            socket = DatagramSocket(Settings.remotePort, InetAddress.getByName(Settings.remoteHost))
            socket.broadcast = true
            val packet = DatagramPacket(buffer, buffer.size)
            socket.receive(packet)
            //textUDP.text = ("" + packet.data) //need to make textout

        } catch (e: Exception) {
            //textUDP.text = "exc" //need to make textout

            e.printStackTrace()
        } finally {
            socket?.close()
        }
    }

    private fun buttonPlayFun() {
        buttonIDPlayPause.setOnClickListener {
            val bText = 90
            sendUDPB(bText)
        }
    } //eo fun

    private fun buttonPrevTrackFun() {
        buttonIDPrevTrack.setOnClickListener {
            val bText = 91
            sendUDPB(bText)
        }
    } //eo fun

    private fun buttonNextTrackFun() {
        buttonIDNextTrack.setOnClickListener {
            val bText = 72
            sendUDPB(bText)
        }
    } //eo fun

    private fun buttonVolUpFun() {
        buttonIDVolumeUp.setOnClickListener {
            val bText = 113
            sendUDPB(bText)
        }
    } //eo fun

    private fun buttonVolDownFun() {
        buttonIDVolumeDown.setOnClickListener {
            val bText = 111
            sendUDPB(bText)
        }
    } //eo fun

    private fun buttonMuteFun() {
        buttonIDMute.setOnClickListener {
            val bText = 112
            sendUDPB(bText)
        }
    } //eo fun
}